#!/usr/bin/env python

import sys

import numpy as np

import jax
import jax.numpy as jnp

import flax
import flax.linen as nn
import flax.optim as optim

if len(sys.argv) != 2:
    print('give me a filename')
    sys.exit(1)

fn = sys.argv[1]

with open(fn) as f:
    configs = np.array([[float(x) for x in l.split()] for l in f.readlines()])

class Autoencoder(nn.Module):
    bottleneck: int
    depth: int

    @nn.compact
    def __call__(self, x):
        pass

loss = None # TODO

key = jax.random.PRNGKey(0)
ae = Autoencoder()
params = ae.init(key, configs)

opt = optim.Adam(learning_rate=1e-2).create(params)
loss_grad = jax.value_and_grad(loss)

while True:
    lval, lgrad = loss_grad(opt.target)
    opt = opt.apply_gradient(lgrad)
    print(f'{lval}')
